'use strict';
module.exports = (sequelize, DataTypes) => {
  const Idea = sequelize.define('Idea', {
    idea: DataTypes.STRING
  }, {});
  Idea.associate = function(models) {
    // associations can be defined here
  };
  return Idea;
};