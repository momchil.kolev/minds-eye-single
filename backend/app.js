const Koa = require("koa")
const bodyParser = require("koa-bodyparser")

const router = require("./routes")

const app = new Koa()
const { BACKEND_PORT: PORT } = process.env

app.use(bodyParser())

app.use(router.routes()).use(router.allowedMethods())

app.use(async ctx => {
	ctx.status = 404
	ctx.body = "Page Not Found"
})

app.listen(PORT, () => console.log(`Server started on port ${PORT}`))
