import React from "react"

import styles from "./Button.module.scss"

export default function Button(props) {
	const { text, type, size, className, onClick } = props
	return (
		<div
			className={`${styles.btn} ${
				type ? styles[`btn-${type}`] : ""
			} ${className || ""} ${size ? styles[`btn-${size}`] : ""}`}
			onClick={onClick}
		>
			{text}
		</div>
	)
}
