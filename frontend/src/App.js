import React, { useEffect, useState } from "react"
import axios from "axios"

import styles from "./App.module.scss"
import CreateIdea from "./components/Card/components/CreateIdea/CreateIdea"
import UpdateDeleteIdea from "./components/Card/components/UpdateDeleteIdea/UpdateDeleteIdea"

const App = props => {
	const [state, setState] = useState([
		{ id: 0, idea: "This is an example idea" }
	])

	useEffect(() => {
		axios.get("/api").then(({ data: ideas }) => {
			setState(ideas)
		})
	}, [])

	return (
		<div className={styles.App}>
			<h1>Mind's Eye</h1>
			<div className={styles.container}>
				<CreateIdea setState={setState} />
				<div className={styles["vertical-line"]} />
				<div className={styles.ideas}>
					{state.map(({ id, idea }) => (
						<UpdateDeleteIdea
							id={id}
							idea={idea}
							key={id}
							setState={setState}
						/>
					))}
				</div>
			</div>
		</div>
	)
}

export default App
